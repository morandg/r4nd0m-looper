/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../logging/LogMacros.h"

#include "TracksPool.h"

namespace rg_looper {

TracksPool::TracksPool(JackClient& jack_client):
    _jack_client(jack_client) {
}

void TracksPool::AddObserver(ITracksObserver& observer) {
  _observers.push_back(observer);
}

void TracksPool::AddTrack() {
  static int next_track_id = 0; // FIXME :D!
  std::stringstream track_name;
  int input_port_id;
  int output_port_id;
  Track::InputState input_state;
  Track::OutputState output_state;

  track_name << "Track " << ++next_track_id;
  input_port_id = _jack_client.AddInputPort(track_name.str() + " input");
  output_port_id = _jack_client.AddOutputPort(track_name.str() + " output");
  if(output_port_id < 0 ||
     input_port_id < 0) {
    LOGER() << "Could not create JACK ports for new track";
    return;
  }

  {
    const std::lock_guard<std::mutex> lock(_tracks_mutex);
    _tracks[next_track_id] =
            std::make_unique<Track>(_jack_client, input_port_id,  output_port_id);

    input_state = _tracks[next_track_id]->GetInputState();
    output_state = _tracks[next_track_id]->GetOutputState();
  }

  for(auto& observer: _observers) {
    observer.get().OnTrackAdded(next_track_id);
  }
  RaiseTrackInputStateChanged(next_track_id, input_state);
  RaiseTrackOutputStateChanged(next_track_id, output_state);
}

void TracksPool::RemoveTrack(int track_id) {
  {
    const std::lock_guard<std::mutex> lock(_tracks_mutex);

    auto found_track = _tracks.find(track_id);
    if (found_track == _tracks.end()) {
      LOGWA() << "BUG? Track ID " << track_id
              << " not found in looper (remove)";
      return;
    }

    _tracks.erase(found_track);
  }

  RaiseTrackRemovedEvent(track_id);
}

void TracksPool::Record(int track_id) {
  {
    const std::lock_guard<std::mutex> lock(_tracks_mutex);

    auto found_track = _tracks.find(track_id);
    if (found_track == _tracks.end()) {
      LOGWA() << "BUG? Track ID " << track_id
              << " not found in looper (remove)";
      return;
    }

    found_track->second->SetInputState(Track::InputState::RECORDING);
  }

  RaiseTrackInputStateChanged(track_id, Track::InputState::RECORDING);
}

void TracksPool::Play(int track_id) {
  {
    const std::lock_guard<std::mutex> lock(_tracks_mutex);

    auto found_track = _tracks.find(track_id);
    if (found_track == _tracks.end()) {
      LOGWA() << "BUG? Track ID " << track_id
              << " not found in looper (remove)";
      return;
    }

    found_track->second->SetInputState(Track::InputState::PLAYING);
  }

  RaiseTrackInputStateChanged(track_id, Track::InputState::PLAYING);
}

void TracksPool::Mute(int track_id, bool is_mute) {
  Track::OutputState new_state = Track::OutputState::MUTED;

  const std::lock_guard<std::mutex> lock(_tracks_mutex);

  {
    auto found_track = _tracks.find(track_id);
    if (found_track == _tracks.end()) {
      LOGWA() << "BUG? Track ID " << track_id
              << " not found in looper (remove)";
      return;
    }

    if (!is_mute) {
      new_state = Track::OutputState::PLAYING;
    }
    found_track->second->SetOutputState(new_state);
  }

  RaiseTrackOutputStateChanged(track_id, new_state);
}

void TracksPool::ClearAllTracks() {
  const std::lock_guard<std::mutex> lock(_tracks_mutex);

  while(!_tracks.empty()) {
    RaiseTrackRemovedEvent(_tracks.begin()->first);
    _tracks.erase(_tracks.begin());
  }
}

void TracksPool::ProcessTracks(jack_nframes_t nframes, IJackPortsBuffer& buffers) {
  const std::lock_guard<std::mutex> lock(_tracks_mutex);

  for(auto& track: _tracks) {
    track.second->OnJackProcess(nframes, buffers);
  }
}

int TracksPool::GetInputPortId(int track_id) {
  const std::lock_guard<std::mutex> lock(_tracks_mutex);

  auto found_track = _tracks.find(track_id);
  if (found_track == _tracks.end()) {
    LOGWA() << "BUG? Track ID " << track_id
            << " not found in looper (output port id)";
    return -1;
  }

  return found_track->second->GetInputPortId();
}

int TracksPool::GetOutputPortId(int track_id) {
  const std::lock_guard<std::mutex> lock(_tracks_mutex);

  auto found_track = _tracks.find(track_id);
  if (found_track == _tracks.end()) {
    LOGWA() << "BUG? Track ID " << track_id
            << " not found in looper (output port id)";
    return -1;
  }

  return found_track->second->GetOutputPortId();
}

void TracksPool::RefreshProgress() {
  const std::lock_guard<std::mutex> lock(_tracks_mutex);

  for(auto& track: _tracks) {
    unsigned long total_usec;
    unsigned long current_usec;

    track.second->BufferProgress(total_usec, current_usec);

    // FIXME: Callback without holding tracks lock
    for(auto& observer: _observers) {
      observer.get().OnTrackProgressChanged(track.first, total_usec, current_usec);
    }
  }
}

void TracksPool::RaiseTrackRemovedEvent(int track_id) {
  for(auto& observer: _observers) {
    observer.get().OnTrackRemoved(track_id);
  }
}

void TracksPool::RaiseTrackInputStateChanged(int track_id, Track::InputState state) {
  for(auto& observer: _observers) {
    observer.get().OnTrackInputStateChanged(track_id, state);
  }
}

void TracksPool::RaiseTrackOutputStateChanged(int track_id, Track::OutputState state) {
  for(auto& observer: _observers) {
    observer.get().OnTrackOutputStateChanged(track_id, state);
  }
}

}       // namespace
