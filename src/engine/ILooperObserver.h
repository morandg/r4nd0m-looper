/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLOPPER_I_LOOPER_OBSERVER_H_
#define _RGLOPPER_I_LOOPER_OBSERVER_H_

#include "ITracksObserver.h"

namespace rg_looper {

class ILooperObserver: public ITracksObserver {
public:
  ILooperObserver() = default;
  virtual ~ILooperObserver() = default;

  virtual void OnJackAudioPortsScanned(
          const std::list<std::string>& input_ports, const std::list<std::string>& output_ports) = 0;
  virtual void OnConnectedInputchanged(int track_id, const std::list<std::string>& connected_input) = 0;
  virtual void OnConnectedOutputchanged(int track_id, const std::list<std::string>& connected_output) = 0;

  // FIXME: The only callback raised from the JACK thread ...  :~p
  virtual void OnJackShutdown() = 0;
};

}       // namespace
#endif  // _RGLOPPER_I_LOOPER_OBSERVER_H_
