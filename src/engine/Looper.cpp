/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sstream>

#include "../logging/LogMacros.h"
#include "Looper.h"

namespace rg_looper {

Looper::Looper():
    _jack_client(*this),
    _tracks(_jack_client) {
}

int Looper::ConnectToJack() {
  _tracks.ClearAllTracks();
  if(_jack_client.Open("R4nd0m-Looper")) {
    return -1;
  }

  return _jack_client.Activate();
}

int Looper::DisconnectFromJack() {
  return _jack_client.Close();
}

void Looper::AddObserver(ILooperObserver& observer) {
  _observers.push_back(observer);
  _tracks.AddObserver(observer);
}

void Looper::AddTrack() {
  _tracks.AddTrack();
}

void Looper::RemoveTrack(int track_id) {
  _tracks.RemoveTrack(track_id);
}

void Looper::Record(int track_id) {
  _tracks.Record(track_id);
}

void Looper::Play(int track_id) {
  _tracks.Play(track_id);
}

void Looper::Mute(int track_id, bool is_mute) {
  _tracks.Mute(track_id, is_mute);
}

void Looper::ScanAudioPorts() {
  std::list<std::string> input_ports;
  std::list<std::string> output_ports;

  if(_jack_client.ScanAudioPorts(input_ports, output_ports)) {
    return;
  }

  // FIXME: Locking
  for(auto& observer: _observers) {
    observer.get().OnJackAudioPortsScanned(input_ports, output_ports);
  }
}

void Looper::ConnectInput(int track_id, const std::string& input) {
  _jack_client.Connect(
          input,
          _jack_client.PortName(_tracks.GetInputPortId(track_id)));
}

void Looper::ConnectOutput(int track_id, const std::string& output) {
  _jack_client.Connect(
          _jack_client.PortName(_tracks.GetOutputPortId(track_id)),
          output);
}

void Looper::ScanConnectedInput(int track_id) {
  std::list<std::string> connected_inputs;

  if(_jack_client.GetConnections(_tracks.GetInputPortId(track_id), connected_inputs)) {
    return;
  }

  // FIXME: Locking
  for(auto& observer: _observers) {
    observer.get().OnConnectedInputchanged(track_id, connected_inputs);
  }
}

void Looper::ScanConnectedOutput(int track_id) {
  std::list<std::string> connected_output;

  if(_jack_client.GetConnections(_tracks.GetOutputPortId(track_id), connected_output)) {
    return;
  }

  // FIXME: Locking
  for(auto& observer: _observers) {
    observer.get().OnConnectedOutputchanged(track_id, connected_output);
  }
}

void Looper::UpdateTracksStatus() {
  _tracks.RefreshProgress();
}

void Looper::OnJackShutdown() {
  // FIXME: Locking
  for(auto& observer: _observers) {
    observer.get().OnJackShutdown();
  }
}

void Looper::OnJackProcess(jack_nframes_t nframes, IJackPortsBuffer& buffers) {
  _tracks.ProcessTracks(nframes, buffers);
}

}       // namespace