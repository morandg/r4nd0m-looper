/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>
#include <cstring>

#include <jack/jack.h>

#include "../logging/LogMacros.h"
#include "Track.h"

namespace rg_looper {

static const int SAMPLES_COUNT = 256 * 1024;

Track::Track(JackClient& jack_client, int input_port_id, int output_port_id):
        _jack_client(jack_client),
        _input_port_id(input_port_id),
        _output_port_id(output_port_id),
        _input_state(InputState::PLAYING),
        _output_state(OutputState::PLAYING){
}

Track::~Track() {
  _jack_client.DeletePort(_input_port_id);
  _jack_client.DeletePort(_output_port_id);
}

void Track::OnJackProcess(jack_nframes_t nframes, IJackPortsBuffer& buffers) {
  jack_default_audio_sample_t* input_buffer;
  jack_default_audio_sample_t* output_buffer;

  input_buffer = buffers.Buffer(_input_port_id, nframes);
  output_buffer = buffers.Buffer(_output_port_id, nframes);

  if(!input_buffer || !output_buffer) {
    return;
  }

  if(_input_state == InputState::RECORDING) {
    _buffer.AppendAudio(input_buffer, nframes);
  }

  if(_output_state == OutputState::MUTED) {
    _buffer.Silence(output_buffer, nframes);
  } else {
    _buffer.FillAudioData(output_buffer, nframes);
  }
}

Track::InputState Track::GetInputState() {
  return _input_state;
}

void Track::SetInputState(Track::InputState state) {
  if(state == InputState::RECORDING) {
    _buffer.Clear();
  }
  _input_state = state;
}

Track::OutputState Track::GetOutputState() {
  return _output_state;
}

void Track::SetOutputState(OutputState state) {
  _output_state = state;
}

int Track::GetInputPortId() {
  return _input_port_id;
}

int Track::GetOutputPortId() {
  return _output_port_id;
}

void Track::BufferProgress(unsigned long& total_usec, unsigned long& current_usec) {
  total_usec = _buffer.Size();
  current_usec = _buffer.Offset();
}

}       // namespace