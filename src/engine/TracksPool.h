/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLOPPER_TRACKS_POOL_H_
#define _RGLOPPER_TRACKS_POOL_H_

#include <mutex>
#include <list>

#include "jack/JackClient.h"
#include "ITracksObserver.h"
#include "Track.h"

namespace rg_looper {

class TracksPool {
public:
  TracksPool(JackClient& jack_client);
  ~TracksPool() = default;

  void AddObserver(ITracksObserver& observer);
  void AddTrack();
  void RemoveTrack(int track_id);
  void Record(int track_id);
  void Play(int track_id);
  void Mute(int track_id, bool is_mute);
  void ClearAllTracks();
  void ProcessTracks(jack_nframes_t nframes, IJackPortsBuffer& buffers);
  int GetInputPortId(int track_id);
  int GetOutputPortId(int track_id);
  void RefreshProgress();

private:
  JackClient& _jack_client;
  std::mutex _tracks_mutex;
  std::unordered_map<int, std::unique_ptr<Track>> _tracks;
  std::list<std::reference_wrapper<ITracksObserver>> _observers;

  void RaiseTrackRemovedEvent(int track_id);
  void RaiseTrackInputStateChanged(int track_id, Track::InputState state);
  void RaiseTrackOutputStateChanged(int track_id, Track::OutputState state);
};

}       // namespace
#endif  // _RGLOPPER_TRACKS_POOL_H_