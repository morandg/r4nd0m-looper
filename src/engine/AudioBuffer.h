/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLOPPER_AUDIO_BUFFER_H_
#define _RGLOPPER_AUDIO_BUFFER_H_

#include <vector>

#include <jack/jack.h>

namespace rg_looper {

class AudioBuffer {
public:
  AudioBuffer();
  ~AudioBuffer() = default;

  void Clear();
  void FillAudioData(jack_default_audio_sample_t* output_buffer, jack_nframes_t  nframes);
  void Silence(jack_default_audio_sample_t* output_buffer, jack_nframes_t nframes);
  void AppendAudio(const jack_default_audio_sample_t* input_buffer, jack_nframes_t nframes);
  jack_nframes_t Size();
  jack_nframes_t Offset();

private:
  unsigned int _read_offset;
  std::vector<jack_default_audio_sample_t> _data;
};

}       // namespace
#endif  // _RGLOPPER_AUDIO_BUFFER_H_
