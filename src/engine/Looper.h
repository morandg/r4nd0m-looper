/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLOPPER_LOOPER_H_
#define _RGLOPPER_LOOPER_H_

#include <list>
#include <functional>

#include "TracksPool.h"
#include "ILooperObserver.h"

namespace rg_looper {

class Looper: private IJackThreadObserver {
public:
  Looper();
  ~Looper() = default;

  int ConnectToJack();
  int DisconnectFromJack();
  void AddObserver(ILooperObserver& observer);
  void AddTrack();
  void RemoveTrack(int track_id);
  void Record(int track_id);
  void Play(int track_id);
  void Mute(int track_id, bool is_mute);
  void ScanAudioPorts();
  void ConnectInput(int track_id, const std::string& input);
  void ConnectOutput(int track_id, const std::string& output);
  void ScanConnectedInput(int track_id);
  void ScanConnectedOutput(int track_id);
  void UpdateTracksStatus();

  // IJackThreadObserver
  virtual void OnJackShutdown() override;
  virtual void OnJackProcess(jack_nframes_t nframes, IJackPortsBuffer& buffers) override;

private:
  JackClient _jack_client;
  TracksPool _tracks;
  std::list<std::reference_wrapper<ILooperObserver>> _observers;
};

}       // namespace
#endif  // _RGLOPPER_JACK_CLIENT_H_
