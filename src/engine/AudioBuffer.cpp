/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cstring>

#include "AudioBuffer.h"

namespace rg_looper {

AudioBuffer::AudioBuffer():
    _read_offset(0) {
}

void AudioBuffer::Clear() {
  _data.clear();
  _read_offset = 0;
}

// TODO: TDD
void AudioBuffer::FillAudioData(jack_default_audio_sample_t* output_buffer, jack_nframes_t  nframes) {
  unsigned int size_to_copy = _data.size() - _read_offset;

  if(_data.empty()) {
    Silence(output_buffer, nframes);
    return;
  }

  if(size_to_copy > nframes) {
    size_to_copy = nframes;
  }

  memcpy(output_buffer, &_data[_read_offset], sizeof(jack_default_audio_sample_t) * size_to_copy);
  _read_offset += size_to_copy;

  // TODO: loop over
  if(_read_offset >= _data.size()) {
    _read_offset = 0;
  }
}

// TODO: TDD
void AudioBuffer::Silence(jack_default_audio_sample_t* output_buffer, jack_nframes_t nframes) {
  memset(output_buffer, 0, nframes * sizeof(jack_default_audio_sample_t));

  _read_offset += nframes;
  if(_read_offset >= _data.size()) {
    _read_offset = 0;
  }
}

// TODO: TDD
void AudioBuffer::AppendAudio(const jack_default_audio_sample_t* input_buffer, jack_nframes_t nframes) {
  std::vector<jack_default_audio_sample_t> new_data;

  new_data.assign(input_buffer, input_buffer + nframes);
  _data.insert(_data.end(), new_data.begin(), new_data.end());
}

jack_nframes_t AudioBuffer::Size() {
  return _data.size();
}

jack_nframes_t AudioBuffer::Offset() {
  return _read_offset;
}

}       // namespace
