/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>

#include "../../logging/LogMacros.h"
#include "JackPort.h"
#include "JackClient.h"

namespace rg_looper {

int JackProcessCallback(jack_nframes_t nframes, void* arg) {
  return static_cast<JackClient*>(arg)->OnJackProcess(nframes);
}

void JackShutdownCallback(void* arg) {
  return static_cast<JackClient*>(arg)->OnJackShutdown();
}

JackClient::JackClient(IJackThreadObserver& observer):
  _jack_client(nullptr),
  _observer(observer),
  _next_port_id(0) {
}

JackClient::~JackClient() {
  CloseLocked();
}

int JackClient::Open(const std::string& name) {
  jack_status_t status;
  int return_code;

  const std::lock_guard<std::mutex> lock(_jack_client_mutex);

  return_code = CloseLocked();
  if(return_code)
    return return_code;

  _jack_client = jack_client_open(name.c_str(), JackNoStartServer, &status);
  if(_jack_client == nullptr) {
    if(status & JackNameNotUnique) {
      LOGER() << "Client \"" << name << "\" already registered to jack";
    } else {
      LOGER() << "Unkown jack open client error!";
    }
    return -1;
  }

  jack_set_process_callback(_jack_client, JackProcessCallback, this);
  jack_on_shutdown(_jack_client, JackShutdownCallback, this);

  LOGIN() << "Connected to jack!";

  return 0;
}

int JackClient::Activate() {
  const std::lock_guard<std::mutex> lock(_jack_client_mutex);

  if(!_jack_client) {
    return -1;
  }

  return jack_activate(_jack_client);
}

int JackClient::Close() {
  const std::lock_guard<std::mutex> lock(_jack_client_mutex);

  return CloseLocked();
}

int JackClient::AddInputPort(const std::string& name) {
  int port_id;
  jack_port_t* new_port;

  const std::lock_guard<std::mutex> lock(_jack_client_mutex);

  new_port = jack_port_register(
          _jack_client, name.c_str(), JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
  if(new_port == nullptr) {
    return -1;
  }
  port_id = NextPortId();

  _created_ports[port_id] = std::make_unique<JackPort>(_jack_client, new_port);

  return port_id;
}

int JackClient::AddOutputPort(const std::string& name) {
  int port_id;
  jack_port_t* new_port;

  const std::lock_guard<std::mutex> lock(_jack_client_mutex);

  new_port = jack_port_register(
          _jack_client, name.c_str(), JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
  if(new_port == nullptr) {
    return -1;
  }
  port_id = NextPortId();

  _created_ports[port_id] = std::make_unique<JackPort>(_jack_client, new_port);

  return port_id;
}

void JackClient::DeletePort(int port_id) {
  const std::lock_guard<std::mutex> lock(_jack_client_mutex);

  auto found_port = _created_ports.find(port_id);
  if(found_port != _created_ports.end()) {
    _created_ports.erase(found_port);
  }
}

std::string JackClient::PortName(int port_id) {
  const std::lock_guard<std::mutex> lock(_jack_client_mutex);

  auto found_port = _created_ports.find(port_id);
  if(found_port == _created_ports.end()) {
    return "";
  }

  return found_port->second->GetName();
}

int JackClient::Connect(const std::string& source_port, const std::string& destination_port) {
  int returned_code;

  const std::lock_guard<std::mutex> lock(_jack_client_mutex);

  LOGDB() << "Connecting " << source_port << " to " << destination_port;
  returned_code = jack_connect(_jack_client, source_port.c_str(), destination_port.c_str());
  if(returned_code) {
    if(returned_code != EEXIST)
      LOGWA() << "Could not connect ports: " << returned_code;
  }

  return returned_code;
}

int JackClient::GetConnections(int port_id, std::list<std::string>& connected_ports) {
  const std::lock_guard<std::mutex> lock(_jack_client_mutex);

  auto found_port = _created_ports.find(port_id);
  if(found_port == _created_ports.end()) {
    LOGWA() << "Pourt not found when scanning connections: " << port_id;
    return -1;
  }

  connected_ports = found_port->second->GetConnections();
  return 0;
}

int JackClient::ScanAudioPorts(std::list<std::string>& input_ports, std::list<std::string>& output_ports) {
  int returned_code;

  returned_code = ScanAudioInputs(input_ports);
  if(returned_code)
    return returned_code;
  return ScanAudioOutputs(output_ports);
}

jack_default_audio_sample_t* JackClient::Buffer(int port_id, jack_nframes_t nframes) {
  // Only called from within JACK thread, lock already acquired
  auto found_port = _created_ports.find(port_id);
  if(found_port != _created_ports.end()) {
    return found_port->second->Buffer(nframes);
  } else {
    return nullptr;
  }
}

int JackClient::OnJackProcess(jack_nframes_t nframes) {
  const std::lock_guard<std::mutex> lock(_jack_client_mutex);
  _observer.OnJackProcess(nframes, *this);

  return 0;
}

void JackClient::OnJackShutdown() {
  LOGWA() << "Jack is shutting down";

  const std::lock_guard<std::mutex> lock(_jack_client_mutex);
  ClearAllPorts();
  _observer.OnJackShutdown();
}

int JackClient::NextPortId() {
  while(_created_ports.find(_next_port_id) != _created_ports.end()) {
    ++_next_port_id;
    // Overflow?
    if(_next_port_id < 0) {
      _next_port_id = 0;
    }
  }

  return _next_port_id;
}

void JackClient::ClearAllPorts() {
  while(!_created_ports.empty()) {
    _created_ports.erase(_created_ports.begin());
  }
}

int JackClient::CloseLocked() {
  int return_code;

  if(!_jack_client) {
    return 0;
  }

  ClearAllPorts();
  return_code = jack_client_close(_jack_client);
  _jack_client = nullptr;

  return return_code;
}

int JackClient::ScanAudioInputs(std::list<std::string>& input_ports) {
  const char** jack_input_ports = GetJackPorts(JackPortIsOutput);

  if(jack_input_ports == nullptr) {
    LOGWA() << "No jack output ports found!";
    return -1;
  }
  ReadJackAudioPorts(jack_input_ports, input_ports);

  return 0;
}

int JackClient::ScanAudioOutputs(std::list<std::string>& output_ports) {
  const char** jack_output_ports = GetJackPorts(JackPortIsInput);

  if(jack_output_ports == nullptr) {
    LOGWA() << "No jack input ports found!";
    return -1;
  }
  ReadJackAudioPorts(jack_output_ports, output_ports);

  return 0;
}

const char** JackClient::GetJackPorts(unsigned long jack_ports_flag) {
  const std::lock_guard<std::mutex> lock(_jack_client_mutex);
  return jack_get_ports(_jack_client, nullptr, nullptr, jack_ports_flag);
}

void JackClient::ReadJackAudioPorts(const char** jack_ports, std::list<std::string>& port) {
  int i = 0;

  assert(jack_ports);

  while(jack_ports[i]) {
    port.push_back(jack_ports[i++]);
  }
  jack_free(jack_ports);
}
}       // namespace
