/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "JackPortsPool.h"
#include "JackPort.h"

namespace rg_looper {

JackPortsPool::JackPortsPool():
    _next_port_id(0) {
}

JackPortsPool::~JackPortsPool() {
}

int JackPortsPool::AddInputPort(jack_client_t* jack_client, const std::string& name) {
  int port_id;
  jack_port_t* new_port;

  new_port = jack_port_register(
          jack_client, name.c_str(), JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
  if(new_port == nullptr) {
    return -1;
  }
  port_id = NextPortId();

  const std::lock_guard<std::mutex> lock(_ports_mutex);
  _created_ports[port_id] = std::make_unique<JackPort>(jack_client, new_port);

  return port_id;
}

int JackPortsPool::AddOutputPort(jack_client_t* jack_client, const std::string& name) {
  int port_id;
  jack_port_t* new_port;

  new_port = jack_port_register(
          jack_client, name.c_str(), JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
  if(new_port == nullptr) {
    return -1;
  }
  port_id = NextPortId();

  const std::lock_guard<std::mutex> lock(_ports_mutex);
  _created_ports[port_id] = std::make_unique<JackPort>(jack_client, new_port);

  return port_id;
}

void JackPortsPool::DeletePort(int port_id) {
  const std::lock_guard<std::mutex> lock(_ports_mutex);

  auto found_port = _created_ports.find(port_id);
  if(found_port != _created_ports.end()) {
    _created_ports.erase(found_port);
  }
}

jack_default_audio_sample_t* JackPortsPool::Buffer(int port_id, jack_nframes_t nframes) {
  jack_default_audio_sample_t* returned_buffer = nullptr;

  const std::lock_guard<std::mutex> lock(_ports_mutex);

  auto found_port = _created_ports.find(port_id);
  if(found_port != _created_ports.end()) {
    returned_buffer = found_port->second->Buffer(nframes);
  }

  return returned_buffer;
}


void JackPortsPool::ClearAll() {
  const std::lock_guard<std::mutex> lock(_ports_mutex);
  while(!_created_ports.empty()) {
    _created_ports.erase(_created_ports.begin());
  }
}

int JackPortsPool::NextPortId() {
  while(_created_ports.find(_next_port_id) != _created_ports.end()) {
    ++_next_port_id;
    // Overflow?
    if(_next_port_id < 0) {
      _next_port_id = 0;
    }
  }

  return _next_port_id;
}

}       // namespace
