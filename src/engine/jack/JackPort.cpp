/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../../logging/LogMacros.h"
#include "JackPort.h"

namespace rg_looper {

JackPort::JackPort(jack_client_t* jack_client, jack_port_t* port):
    _jack_client(jack_client),
    _port(port) {
}

JackPort::~JackPort() {
  jack_port_unregister(_jack_client, _port);
}

jack_default_audio_sample_t* JackPort::Buffer(jack_nframes_t nframes) {
  return static_cast<jack_default_audio_sample_t*>(
          jack_port_get_buffer(_port, nframes));
}

std::string JackPort::GetName() {
  return jack_port_name(_port);
}

std::list<std::string> JackPort::GetConnections() {
  const char** jack_ports;
  std::list<std::string> connections;
  int i = 0;

  jack_ports = jack_port_get_connections(_port);
  if(jack_ports == nullptr) {
    LOGWA() << "Could not get jack connected ports!";
    return connections;
  }

  while(jack_ports[i]) {
    connections.push_back(jack_ports[i++]);
  }
  jack_free(jack_ports);

  return connections;
}

}       // namespace