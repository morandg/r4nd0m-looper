/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLOPPER_JACK_INPUT_PORT_H_
#define _RGLOPPER_JACK_INPUT_PORT_H_

#include "IJackPort.h"

namespace rg_looper {

class JackPort: public IJackPort {
public:
  JackPort(jack_client_t* jack_client, jack_port_t* _port);
  ~JackPort();

  // IJackPort
  virtual jack_default_audio_sample_t* Buffer(jack_nframes_t nframes) override;
  virtual std::string GetName() override;
  virtual std::list<std::string> GetConnections() override;

private:
  jack_client_t* _jack_client;
  jack_port_t* _port;
};

}       // namespace
#endif  // _RGLOPPER_JACK_INPUT_PORT_H_
