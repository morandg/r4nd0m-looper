/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLOPPER_JACK_CLIENT_H_
#define _RGLOPPER_JACK_CLIENT_H_

#include <memory>
#include <unordered_map>
#include <list>
#include <mutex>

#include "IJackThreadObserver.h"
#include "IJackPort.h"

namespace rg_looper {

class JackClient: private IJackPortsBuffer {
public:
  JackClient(IJackThreadObserver& observer);
  ~JackClient();

  int Open(const std::string& name);
  int Activate();
  int Close();
  int AddInputPort(const std::string& name);
  int AddOutputPort(const std::string& name);
  void DeletePort(int port_id);
  int ScanAudioPorts(std::list<std::string>& input_ports, std::list<std::string>& output_ports);
  std::string PortName(int port_id);
  int Connect(const std::string& source_port, const std::string& destination_port);
  int GetConnections(int port_id, std::list<std::string>& connected_ports);

  // IJackPortsBuffer
  jack_default_audio_sample_t* Buffer(int port_id, jack_nframes_t nframes);

private:
  jack_client_t* _jack_client;
  IJackThreadObserver& _observer;
  int _next_port_id;
  std::mutex _jack_client_mutex;
  std::unordered_map<int, std::unique_ptr<IJackPort>> _created_ports;

  int OnJackProcess(jack_nframes_t nframes);
  void OnJackShutdown();
  int NextPortId();
  void ClearAllPorts();
  int CloseLocked();
  int ScanAudioInputs(std::list<std::string>& input_ports);
  int ScanAudioOutputs(std::list<std::string>& output_ports);
  const char** GetJackPorts(unsigned long jack_ports_flag);
  void ReadJackAudioPorts(const char** jack_ports, std::list<std::string>& port);

  friend int JackProcessCallback(jack_nframes_t, void*);
  friend void JackShutdownCallback(void*);
};

}       // namespace
#endif  // _RGLOPPER_JACK_CLIENT_H_
