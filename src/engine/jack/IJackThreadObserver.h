/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLOPPER_I_JACK_THREAD_OBSERVER_H_
#define _RGLOPPER_I_JACK_THREAD_OBSERVER_H_

#include <jack/jack.h>

#include "IJackPortsBuffer.h"

namespace rg_looper {

class IJackThreadObserver {
public:
  IJackThreadObserver() = default;
  virtual ~IJackThreadObserver() = default;

  virtual void OnJackShutdown() = 0;
  virtual void OnJackProcess(jack_nframes_t nframes, IJackPortsBuffer& buffers) = 0;
};

}       // namespace
#endif  // _RGLOPPER_I_JACK_THREAD_OBSERVER_H_
