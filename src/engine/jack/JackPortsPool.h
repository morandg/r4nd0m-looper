/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLOPPER_JACK_PORTS_POOL_H_
#define _RGLOPPER_JACK_PORTS_POOL_H_

#include <string>
#include <unordered_map>
#include <memory>
#include <mutex>

#include "IJackPort.h"

namespace rg_looper {

class JackPortsPool {
public:
  JackPortsPool();
  ~JackPortsPool();

  int AddInputPort(jack_client_t* jack_client, const std::string& name);
  int AddOutputPort(jack_client_t* jack_client, const std::string& name);
  jack_default_audio_sample_t* Buffer(int port_id, jack_nframes_t nframes);
  void DeletePort(int port_id);
  void ClearAll();

private:
  int _next_port_id;
  std::mutex _ports_mutex;
  std::unordered_map<int, std::unique_ptr<IJackPort>> _created_ports;

  int NextPortId();
};

}       // namespace
#endif  // _RGLOPPER_JACK_CLIENT_H_
