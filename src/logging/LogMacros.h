/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLOOPER_LOG_MACROS_HPP_
#define _RGLOOPER_LOG_MACROS_HPP_

#include "LoggerInstance.h"

#define LOGDB()     (*rg_looper::LoggerInstance::getInstance()).getLogStream(rg_looper::ILogSink::DEBUG).get()
#define LOGIN()     (*rg_looper::LoggerInstance::getInstance()).getLogStream(rg_looper::ILogSink::INFO).get()
#define LOGWA()     (*rg_looper::LoggerInstance::getInstance()).getLogStream(rg_looper::ILogSink::WARNING).get()
#define LOGER()     (*rg_looper::LoggerInstance::getInstance()).getLogStream(rg_looper::ILogSink::ERROR).get()

#endif  // _RGLOOPER_LOG_MACROS_HPP_
