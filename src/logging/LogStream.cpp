/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "LogStream.h"

namespace rg_looper {

LogStream::LogStream(ILogSink& log_sink, ILogSink::LogLevel log_level) :
    _log_sink(log_sink),
    _log_level(log_level) {
}

LogStream::LogStream(const LogStream& rhs) :
    _log_sink(rhs._log_sink),
    _log_level(rhs._log_level) {
}

LogStream::~LogStream() {
  _log_sink.sinkLogLine(_log_level, _stringstream.str());
}

std::ostream& LogStream::get() {
  return _stringstream;
}

}       // namespace
