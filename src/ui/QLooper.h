/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLOOPER_Q_LOOPER_HPP_
#define _RGLOOPER_Q_LOOPER_HPP_

#include <QObject>

#include "../engine/Looper.h"
#include "events/LooperEventsQueue.h"

namespace rg_looper {

class QLooper:
        public QObject,
        private ILooperObserver {
  Q_OBJECT

public:
  explicit QLooper(Looper& looper, QObject *parent = 0);
  virtual ~QLooper() = default;

  // ILooperObserver
  virtual void OnJackShutdown() override;
  virtual void OnTrackRemoved(int track_id) override;
  virtual void OnTrackAdded(int track_id) override;
  virtual void OnTrackInputStateChanged(int track_id, Track::InputState state) override;
  virtual void OnTrackOutputStateChanged(int track_id, Track::OutputState state) override;
  virtual void OnJackAudioPortsScanned(
          const std::list<std::string>& input_ports, const std::list<std::string>& output_ports) override;
  virtual void OnConnectedInputchanged(int track_id, const std::list<std::string>& connected_input) override;
  virtual void OnConnectedOutputchanged(int track_id, const std::list<std::string>& connected_output) override;
  virtual void OnTrackProgressChanged(int track_id, unsigned long total_usec, unsigned long current_usec) override;

signals:
  void ConnectedToJack();
  void JackConnectionFailed();
  void JackConnectionClosed();
  void JackShutdown();
  void TrackAdded(int track_id);
  void TrackRemoved(int track_id);
  void TrackInputStateChanged(int track_id, Track::InputState new_state);
  void TrackOutputStateChanged(int track_id, Track::OutputState new_state);
  void AudioPortsScanned(
          const std::list<std::string>& input_ports, const std::list<std::string>& output_ports);
  void ConnectedInputchanged(int track_id, const std::list<std::string>& connected_input);
  void ConnectedOutputchanged(int track_id, const std::list<std::string>& connected_output);
  void TrackProgressChanged(int track_id, unsigned long total_usec, unsigned long current_usec);

public slots:
  void ConnectToJack();
  void DisconnectFromJack();
  void AddTrack();
  void RemoveTrack(int track_id);
  void Record(int track_id);
  void Play(int track_id);
  void Mute(int track_id, bool is_mute);
  void ScanAudioPorts();
  void ConnectInput(int track_id, const std::string& input);
  void ConnectOutput(int track_id, const std::string& output);
  void ScanConnectedInput(int track_id);
  void ScanConnectedOutput(int track_id);
  void UpdateTracksStatus();

private:
  Looper& _looper;
  LooperEventsQueue _looper_events_queue;
};

}       // namespace
#endif  // _RGLOOPER_Q_LOOPER_HPP_
