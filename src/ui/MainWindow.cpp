/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QCoreApplication>
#include <QMenuBar>
#include <QMessageBox>

#include "MainWindow.h"

namespace rg_looper {

MainWindow::MainWindow(Looper& looper, QWidget *parent) :
      QMainWindow(parent),
      _q_looper(new QLooper(looper, this)),
      _tracks_panel(new TracksPanel(*_q_looper, this)) {
  BuildMenu();
  show();

  setCentralWidget(_tracks_panel);

  connect(_q_looper, &QLooper::JackShutdown,
          this, &MainWindow::OnJackShutdown);
  connect(_q_looper, &QLooper::JackConnectionFailed,
          this, &MainWindow::OnJackConnectionFailed);
  connect(_q_looper, &QLooper::JackConnectionClosed,
          this, &MainWindow::OnJackConnectionClosed);
  connect(_q_looper, &QLooper::ConnectedToJack,
          this, &MainWindow::OnConnectedToJack);

  _q_looper->ConnectToJack();
}

MainWindow::~MainWindow() {
  _q_looper->DisconnectFromJack();
}

void MainWindow::OnConnectedToJack() {
  EnableConnectToJack(false);
}

void MainWindow::OnJackConnectionFailed() {
  QMessageBox::warning(this, "JACK connection", "JACK connection failed");
  EnableConnectToJack(true);
}

void MainWindow::OnJackShutdown() {
  QMessageBox::warning(this, "JACK connection", "JACK is shutting down");
  EnableConnectToJack(true);
}

void MainWindow::OnJackConnectionClosed() {
  EnableConnectToJack(true);
}

void MainWindow::BuildMenu() {
  QMenuBar* menu_bar = new QMenuBar(this);
  setMenuBar(menu_bar);

  // File
  QMenu* menu_file = menu_bar->addMenu("&File");

  // -- Quit
  QAction* exit_item = new QAction("&Exit", this);
  connect(exit_item, &QAction::triggered,
          QCoreApplication::instance(), &QCoreApplication::quit);
  menu_file->addAction(exit_item);

  // JACK
  QMenu* menu_jack = menu_bar->addMenu("JACK");

  // -- Connect
  _connect_to_jack_item = new QAction("Connect", this);
  connect(_connect_to_jack_item, &QAction::triggered,
          _q_looper, &QLooper::ConnectToJack);
  menu_jack->addAction(_connect_to_jack_item);

  // -- Disconnect
  _disconnect_from_jack_item = new QAction("Disconnect", this);
  connect(_disconnect_from_jack_item, &QAction::triggered,
          _q_looper, &QLooper::DisconnectFromJack);
  menu_jack->addAction(_disconnect_from_jack_item);
  _disconnect_from_jack_item->setEnabled(false);

  // -- Scan ports
  _scan_jack_ports_item = new QAction("Rescan audio ports", this);
  connect(_scan_jack_ports_item, &QAction::triggered,
          _q_looper, &QLooper::ScanAudioPorts);
  menu_jack->addAction(_scan_jack_ports_item);
  _scan_jack_ports_item->setEnabled(false);

  // Tracks
  QMenu* menu_tracks = menu_bar->addMenu("Track");

  // -- Add
  _add_track_item = new QAction("Add", this);
  connect(_add_track_item, &QAction::triggered,
          _q_looper, &QLooper::AddTrack);
  menu_tracks->addAction(_add_track_item);
  _add_track_item->setEnabled(false);
}

void MainWindow::EnableConnectToJack(bool is_enable) {
  _connect_to_jack_item->setEnabled(is_enable);
  _disconnect_from_jack_item->setEnabled(!is_enable);
  _add_track_item->setEnabled(!is_enable);
  _scan_jack_ports_item->setEnabled(!is_enable);
}

}       // namespace
