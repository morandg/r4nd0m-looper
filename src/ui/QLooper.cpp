/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../logging/LogMacros.h"
#include "QLooper.h"

namespace rg_looper {

QLooper::QLooper(Looper& looper, QObject *parent):
    QObject(parent),
    _looper(looper),
    _looper_events_queue(_looper, *this) {
}

void QLooper::OnJackShutdown() {
  emit JackShutdown();
}

void QLooper::OnTrackRemoved(int track_id) {
  emit TrackRemoved(track_id);
}

void QLooper::OnTrackAdded(int track_id) {
  emit TrackAdded(track_id);
}

void QLooper::OnTrackInputStateChanged(int track_id, Track::InputState state) {
  emit TrackInputStateChanged(track_id, state);
}

void QLooper::OnTrackOutputStateChanged(int track_id, Track::OutputState state) {
  emit TrackOutputStateChanged(track_id, state);
}

void QLooper::OnJackAudioPortsScanned(
        const std::list<std::string>& input_ports, const std::list<std::string>& output_ports) {
  emit AudioPortsScanned(input_ports, output_ports);
}

void QLooper::OnConnectedInputchanged(int track_id, const std::list<std::string>& connected_input) {
  emit ConnectedInputchanged(track_id, connected_input);
}

void QLooper::OnConnectedOutputchanged(int track_id, const std::list<std::string>& connected_output) {
  emit ConnectedOutputchanged(track_id, connected_output);
}

void QLooper::OnTrackProgressChanged(int track_id, unsigned long total_usec, unsigned long current_usec) {
  emit TrackProgressChanged(track_id, total_usec, current_usec);
}

void QLooper::ConnectToJack() {
  // FIXME: Background thread!?
  if(_looper.ConnectToJack()) {
    emit JackConnectionFailed();
  } else {
    emit ConnectedToJack();
  }
}

void QLooper::DisconnectFromJack() {
  _looper.DisconnectFromJack();
  emit JackConnectionClosed();
}

void QLooper::AddTrack() {
  _looper.AddTrack();
}

void QLooper::RemoveTrack(int track_id) {
  _looper.RemoveTrack(track_id);
}

void QLooper::Record(int track_id) {
  _looper.Record(track_id);
}

void QLooper::Play(int track_id) {
  _looper.Play(track_id);
}

void QLooper::Mute(int track_id, bool is_mute) {
  _looper.Mute(track_id, is_mute);
}

void QLooper::ScanAudioPorts() {
  _looper.ScanAudioPorts();
}

void QLooper::ConnectInput(int track_id, const std::string& input) {
  _looper.ConnectInput(track_id, input);
}

void QLooper::ConnectOutput(int track_id, const std::string& output) {
  _looper.ConnectOutput(track_id, output);
}

void QLooper::ScanConnectedInput(int track_id) {
  _looper.ScanConnectedInput(track_id);
}

void QLooper::ScanConnectedOutput(int track_id) {
  _looper.ScanConnectedOutput(track_id);
}

void QLooper::UpdateTracksStatus() {
  _looper.UpdateTracksStatus();
}

}       // namespace
