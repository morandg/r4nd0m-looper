/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLOOPER_MAIN_WINDOW_HPP_
#define _RGLOOPER_MAIN_WINDOW_HPP_

#include <QMainWindow>

#include "QLooper.h"
#include "tracks/TracksPanel.h"

namespace rg_looper {

class MainWindow: public QMainWindow {
  Q_OBJECT

public:
  explicit MainWindow(Looper& looper, QWidget *parent = 0);
  virtual ~MainWindow();

private slots:
  void OnConnectedToJack();
  void OnJackConnectionFailed();
  void OnJackConnectionClosed();
  void OnJackShutdown();

private:
  QLooper* _q_looper;
  TracksPanel* _tracks_panel;
  QAction* _connect_to_jack_item;
  QAction* _disconnect_from_jack_item;
  QAction* _scan_jack_ports_item;
  QAction* _add_track_item;

  void BuildMenu();
  void EnableConnectToJack(bool is_enable);
};

}       // namespace
#endif  // _RGLOOPER_MAIN_WINDOW_HPP_
