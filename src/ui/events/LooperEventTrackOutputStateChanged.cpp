/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "LooperEventTrackOutputStateChanged.h"

namespace rg_looper {

LooperEventTrackOutputStateChanged::LooperEventTrackOutputStateChanged(
        int track_id, Track::OutputState state):
    _track_id(track_id),
    _state(state){
}

void LooperEventTrackOutputStateChanged::Execute(ILooperObserver& callbacks) {
  callbacks.OnTrackOutputStateChanged(_track_id, _state);
}

}       // namespace