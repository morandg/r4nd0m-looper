/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLOPPER_LOOPER_EVENTS_QUEUE_H_
#define _RGLOPPER_LOOPER_EVENTS_QUEUE_H_

#include <memory>
#include <queue>

#include <QObject>
#include <QSocketNotifier>

#include "../../engine/Looper.h"
#include "ILooperEvent.h"

namespace rg_looper {

class LooperEventsQueue:
        public QObject,
        public ILooperObserver {
  Q_OBJECT

public:
  explicit LooperEventsQueue(Looper& looper, ILooperObserver& observer);
  virtual ~LooperEventsQueue() = default;

   // ILooperObserver
  virtual void OnJackShutdown() override;
  virtual void OnTrackRemoved(int track_id) override;
  virtual void OnTrackAdded(int track_id) override;
  virtual void OnTrackInputStateChanged(int track_id, Track::InputState state) override;
  virtual void OnTrackOutputStateChanged(int track_id, Track::OutputState state) override;
  virtual void OnJackAudioPortsScanned(
          const std::list<std::string>& input_ports, const std::list<std::string>& output_ports) override;
  virtual void OnConnectedInputchanged(int track_id, const std::list<std::string>& connected_input) override;
  virtual void OnConnectedOutputchanged(int track_id, const std::list<std::string>& connected_output) override;
  virtual void OnTrackProgressChanged(int track_id, unsigned long total_usec, unsigned long current_usec) override;

private slots:
    void LooperEventQueued();

private:
  std::queue<std::unique_ptr<ILooperEvent>> _events_queue;
  ILooperObserver& _observer;
  QSocketNotifier* _socket_notifier;
  int _self_pipe[2];

  void PushEvent(std::unique_ptr<ILooperEvent>& event);
  void WakeupeQtThread();
  void AckLooperEvent();
  void ExecuteNextEvent();
};

}       // namespace
#endif  // _RGLOPPER_LOOPER_EVENTS_QUEUE_H_
