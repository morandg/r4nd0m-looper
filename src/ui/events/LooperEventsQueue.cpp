/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sys/socket.h>

#include <unistd.h>

#include "../../logging/LogMacros.h"
#include "LooperEventShutdown.h"
#include "LooperEventTrackRemoved.h"
#include "LooperEventTrackAdded.h"
#include "LooperEventTrackInputStateChanged.h"
#include "LooperEventTrackOutputStateChanged.h"
#include "LooperEventAudioPortsScanned.h"
#include "LooperEventConnectedInputChanged.h"
#include "LooperEventConnectedOutputChanged.h"
#include "LooperEventTrackProgressChanged.h"
#include "LooperEventsQueue.h"

namespace rg_looper {

LooperEventsQueue::LooperEventsQueue(Looper& looper, ILooperObserver& observer):
    _observer(observer) {
  if(::socketpair(AF_UNIX, SOCK_STREAM, 0, _self_pipe)) {
    LOGER() << "Could not create thread synchronization socket pair!";
    return;
  }
  _socket_notifier = new QSocketNotifier(_self_pipe[1], QSocketNotifier::Read, this);
  connect(_socket_notifier, &QSocketNotifier::activated,
      this, &LooperEventsQueue::LooperEventQueued);
  looper.AddObserver(*this);
}

void LooperEventsQueue::OnJackShutdown() {
  std::unique_ptr<ILooperEvent> event = std::make_unique<LooperEventShutdown>();
  PushEvent(event);
}

void LooperEventsQueue::OnTrackRemoved(int track_id) {
  std::unique_ptr<ILooperEvent> event = std::make_unique<LooperEventTrackRemoved>(track_id);
  PushEvent(event);
}

void LooperEventsQueue::OnTrackAdded(int track_id) {
  std::unique_ptr<ILooperEvent> event = std::make_unique<LooperEventTrackAdded>(track_id);
  PushEvent(event);
}

void LooperEventsQueue::OnTrackInputStateChanged(int track_id, Track::InputState state) {
  std::unique_ptr<ILooperEvent> event = std::make_unique<LooperEventTrackInputStateChanged>(track_id, state);
  PushEvent(event);
}

void LooperEventsQueue::OnTrackOutputStateChanged(int track_id, Track::OutputState state) {
  std::unique_ptr<ILooperEvent> event = std::make_unique<LooperEventTrackOutputStateChanged>(track_id, state);
  PushEvent(event);
}

void LooperEventsQueue::OnJackAudioPortsScanned(
        const std::list<std::string>& input_ports, const std::list<std::string>& output_ports) {
  std::unique_ptr<ILooperEvent> event = std::make_unique<LooperEventAudioPortsScanned>(input_ports, output_ports);
  PushEvent(event);
}

void LooperEventsQueue::OnConnectedInputchanged(int track_id, const std::list<std::string>& connected_input) {
  std::unique_ptr<ILooperEvent> event = std::make_unique<LooperEventConnectedInputChanged>(track_id, connected_input);
  PushEvent(event);
}

void LooperEventsQueue::OnConnectedOutputchanged(int track_id, const std::list<std::string>& connected_output) {
  std::unique_ptr<ILooperEvent> event = std::make_unique<LooperEventConnectedOutputChanged>(track_id, connected_output);
  PushEvent(event);
}

void LooperEventsQueue::OnTrackProgressChanged(int track_id, unsigned long total_usec, unsigned long current_usec) {
  std::unique_ptr<ILooperEvent> event =
          std::make_unique<LooperEventTrackPgroessChanged>(track_id, total_usec, current_usec);
  PushEvent(event);
}

void LooperEventsQueue::LooperEventQueued() {
  _socket_notifier->setEnabled(false);
  AckLooperEvent();
  ExecuteNextEvent();
  _socket_notifier->setEnabled(true);
}

void LooperEventsQueue::PushEvent(std::unique_ptr<ILooperEvent>& event) {
  _events_queue.push(std::move(event));
  WakeupeQtThread();
}

void LooperEventsQueue::WakeupeQtThread() {
  int written_size;
  char buffer = 'a';

  written_size = ::write(_self_pipe[0], &buffer, sizeof(buffer));
  if(written_size <= 0) {
    LOGER() << "Could not wake up QT thread: " << strerror(errno);
  }
}

void LooperEventsQueue::AckLooperEvent() {
  char buffer;
  int read_size;

  read_size = ::read(_self_pipe[1], &buffer, sizeof(buffer));
  if(read_size <= 0) {
    LOGER() << "Could not ack looper thread event: " << strerror(errno);
  }
}

void LooperEventsQueue::ExecuteNextEvent() {
  if(_events_queue.empty()) {
    LOGWA() << "BUG? Empty looper event queue";
    return;
  }
  
  std::unique_ptr<ILooperEvent> event = std::move(_events_queue.front());
  _events_queue.pop();
  event->Execute(_observer);
}


}       // namespace