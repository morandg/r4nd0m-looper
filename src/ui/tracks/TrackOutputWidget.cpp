/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QWidget>
#include <QVBoxLayout>

#include "../QLooper.h"
#include "TrackOutputWidget.h"

namespace rg_looper {

TrackOutputWidget::TrackOutputWidget(QLooper& q_looper, int track_id, QWidget* parent) :
        QWidget(parent),
        _q_looper(q_looper),
        _track_id(track_id),
        _output_state(Track::OutputState::MUTED),
        _mute_button(new QPushButton(this)) {
  QVBoxLayout* layout = new QVBoxLayout(this);

  layout->addWidget(_mute_button);
  connect(_mute_button, &QPushButton::clicked,
          this, &TrackOutputWidget::MuteClicked);
  TrackOutputStateChanged(_output_state);

  _connected_output_ports = new QComboBox(this);
  layout->addWidget(_connected_output_ports);

  _output_ports = new QComboBox(this);
  layout->addWidget(_output_ports);
  connect(_output_ports, SIGNAL(activated(int)),
          this, SLOT(OutputPortSelected(int)));
  TrackOutputStateChanged(_output_state);
}

void TrackOutputWidget::TrackOutputStateChanged(Track::OutputState state) {
  if(state == Track::OutputState::MUTED) {
    _mute_button->setText("Unmute");
  } else {
    _mute_button->setText("Mute");
  }

  _output_state = state;
}

void TrackOutputWidget::TrackConnectedOutputChanged(const std::list<std::string>& connected_outputs) {
  _connected_output_ports->clear();
  for(auto connected_output: connected_outputs) {
    _connected_output_ports->addItem(connected_output.c_str());
  }
}

void TrackOutputWidget::AudioPortsScanned(const std::list<std::string>& output_ports) {
  _output_ports->clear();
  for(auto& input_port: output_ports) {
    _output_ports->addItem(input_port.c_str());
  }
}

void TrackOutputWidget::MuteClicked() {
  _q_looper.Mute(_track_id, _output_state == Track::OutputState::PLAYING);
}

void TrackOutputWidget::OutputPortSelected(int item_id) {
  _q_looper.ConnectOutput(_track_id, _output_ports->itemText(item_id).toStdString());
  _q_looper.ScanConnectedOutput(_track_id);
}

}       // namespace
