/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLOOPER_TRACK_INPUT_WIDGET_HPP_
#define _RGLOOPER_TRACK_INPUT_WIDGET_HPP_

#include <QWidget>
#include <QComboBox>
#include <QPushButton>

#include "../QLooper.h"

namespace rg_looper {

class TrackInputWidget: public QWidget {
  Q_OBJECT

public:
  explicit TrackInputWidget(QLooper& q_looper, int track_id, QWidget* parent = 0);
  virtual ~TrackInputWidget() = default;

public slots:
  void TrackInputStateChanged(Track::InputState state);
  void TrackConnectedInputChanged(const std::list<std::string>& connected_inputs);
  void AudioPortsScanned(const std::list<std::string>& input_ports);

private slots:
  void RecordPlayClicked();
  void InputPortSelected(int item_id);

private:
  QLooper& _q_looper;
  int _track_id;
  Track::InputState _input_state;
  QPushButton* _record_play_button;
  QComboBox* _input_ports;
  QComboBox* _connected_input_ports;
};

}       // namespace
#endif  // _RGLOOPER_TRACK_WIDGET_HPP_
