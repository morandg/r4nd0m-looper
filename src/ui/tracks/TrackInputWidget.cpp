/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QVBoxLayout>

#include "TrackInputWidget.h"

namespace rg_looper {

TrackInputWidget::TrackInputWidget(QLooper& q_looper, int track_id, QWidget* parent):
      QWidget(parent),
      _q_looper(q_looper),
      _track_id(track_id),
      _input_state(Track::InputState::PLAYING),
      _record_play_button(new QPushButton(this)) {
  QVBoxLayout* layout = new QVBoxLayout(this);

  layout->addWidget(_record_play_button);
  connect(_record_play_button, &QPushButton::clicked,
          this, &TrackInputWidget::RecordPlayClicked);

  _connected_input_ports = new QComboBox(this);
  layout->addWidget(_connected_input_ports);

  _input_ports = new QComboBox(this);
  connect(_input_ports, SIGNAL(activated(int)),
          this, SLOT(InputPortSelected(int)));
  layout->addWidget(_input_ports);

  TrackInputStateChanged(_input_state);
}

void TrackInputWidget::TrackInputStateChanged(Track::InputState state) {
  if(state == Track::InputState::PLAYING) {
    _record_play_button->setText("Record");
  } else {
    _record_play_button->setText("Play");
  }
  _input_state = state;
}

void TrackInputWidget::TrackConnectedInputChanged(const std::list<std::string>& connected_inputs) {
  _connected_input_ports->clear();
  for(auto connected_input: connected_inputs) {
    _connected_input_ports->addItem(connected_input.c_str());
  }
}

void TrackInputWidget::AudioPortsScanned(const std::list<std::string>& input_ports) {
  _input_ports->clear();
  for(auto& input_port: input_ports) {
    _input_ports->addItem(input_port.c_str());
  }
}

void TrackInputWidget::RecordPlayClicked() {
  if (_input_state == Track::InputState::PLAYING) {
    _q_looper.Record(_track_id);
  } else {
    _q_looper.Play(_track_id);
  }
}

void TrackInputWidget::InputPortSelected(int item_id) {
  _q_looper.ConnectInput(_track_id, _input_ports->itemText(item_id).toStdString());
  _q_looper.ScanConnectedInput(_track_id);
}

}       // namespace
