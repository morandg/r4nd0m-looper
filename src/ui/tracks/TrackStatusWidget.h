/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLOOPER_TRACK_STATUS_WIDGET_HPP_
#define _RGLOOPER_TRACK_STATUS_WIDGET_HPP_

#include <QWidget>
#include <QProgressBar>

namespace rg_looper {

class TrackStatusWidget: public QWidget {
  Q_OBJECT

public:
  explicit TrackStatusWidget(QWidget* parent = 0);
  virtual ~TrackStatusWidget() = default;

public slots:
  void ProgressChanged(unsigned long total_usec, unsigned long current_usec);

private:
  QProgressBar* _progress_bar;
};

}       // namespace
#endif  // _RGLOOPER_TRACK_STATUS_WIDGET_HPP_
