/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QMessageBox>

#include "../../logging/LogMacros.h"
#include "TracksPanel.h"

namespace rg_looper {

TracksPanel::TracksPanel(QLooper& q_looper, QWidget* parent):
      QWidget(parent),
      _q_looper(q_looper),
      _refresh_tracks_timer(new QTimer(this)) {
  QGridLayout* main_layout = new QGridLayout(this);
  QWidget* tracks_widget = new QWidget(this);
  _tracks_layout = new QVBoxLayout(tracks_widget);

  tracks_widget->setLayout(_tracks_layout);
  main_layout->addWidget(tracks_widget, 0, 0);
  main_layout->setRowStretch(1, 1);

  _refresh_tracks_timer->setInterval(100);
  connect(_refresh_tracks_timer, &QTimer::timeout,
          &_q_looper, &QLooper::UpdateTracksStatus);

  connect(&_q_looper, &QLooper::TrackAdded,
          this, &TracksPanel::TrackAdded);
  connect(&_q_looper, &QLooper::TrackRemoved,
          this, &TracksPanel::TrackRemoved);
  connect(&_q_looper, &QLooper::TrackInputStateChanged,
          this, &TracksPanel::TrackInputStateChanged);
  connect(&_q_looper, &QLooper::TrackOutputStateChanged,
          this, &TracksPanel::TrackOutputStateChanged);
  connect(&q_looper, &QLooper::ConnectedOutputchanged,
          this, &TracksPanel::ConnectedOutputchanged);
  connect(&q_looper, &QLooper::ConnectedInputchanged,
          this, &TracksPanel::ConnectedInputchanged);
  connect(&q_looper, &QLooper::ConnectedToJack,
          this, &TracksPanel::ConnectedToJack);
  connect(&q_looper, &QLooper::JackConnectionClosed,
          this, &TracksPanel::JackConnectionClosed);
  connect(&q_looper, &QLooper::TrackProgressChanged,
          this, &TracksPanel::TrackProgressChanged);

  setLayout(main_layout);
}

void TracksPanel::TrackAdded(int track_id) {
  _tracks[track_id] = new TrackWidget(_q_looper, track_id, this);
  _tracks_layout->addWidget(_tracks[track_id]);
  _q_looper.ScanAudioPorts();
}

void TracksPanel::TrackRemoved(int track_id) {
  auto found_track = _tracks.find(track_id);

  if(found_track == _tracks.end()) {
    LOGWA() << "Track widget " << track_id << " not found (remove)";
    return;
  }

  _tracks_layout->removeWidget(*found_track);
  delete *found_track;
  _tracks.erase(found_track);

  _q_looper.ScanAudioPorts();
}

void TracksPanel::TrackInputStateChanged(int track_id, Track::InputState state) {
  auto found_track = _tracks.find(track_id);

  if(found_track == _tracks.end()) {
    LOGWA() << "Track widget " << track_id << " not found (input state changed)";
    return;
  }
  found_track.value()->TrackInputStateChanged(state);
}

void TracksPanel::TrackOutputStateChanged(int track_id, Track::OutputState state) {
  auto found_track = _tracks.find(track_id);

  if(found_track == _tracks.end()) {
    LOGWA() << "Track widget " << track_id << " not found (output state changed)";
    return;
  }
  found_track.value()->TrackOutputStateChanged(state);
}

void TracksPanel::ConnectedInputchanged(int track_id, const std::list<std::string>& connected_input) {
  auto found_track = _tracks.find(track_id);

  if(found_track == _tracks.end()) {
    LOGWA() << "Track widget " << track_id << " not found (connected inputs changed)";
    return;
  }
  found_track.value()->TrackConnectedInputChanged(connected_input);
}

void TracksPanel::ConnectedOutputchanged(int track_id, const std::list<std::string>& connected_output) {
  auto found_track = _tracks.find(track_id);

  if(found_track == _tracks.end()) {
    LOGWA() << "Track widget " << track_id << " not found (connected outputs changed)";
    return;
  }
  found_track.value()->TrackConnectedOutputChanged(connected_output);
}

void TracksPanel::ConnectedToJack() {
  _refresh_tracks_timer->start();
}

void TracksPanel::JackConnectionClosed() {
  _refresh_tracks_timer->stop();
}

void TracksPanel::TrackProgressChanged(int track_id, unsigned long total_usec, unsigned long current_usec) {
  auto found_track = _tracks.find(track_id);

  if(found_track == _tracks.end()) {
    LOGWA() << "Track widget " << track_id << " not found (progress changed)";
    return;
  }

  found_track.value()->TrackProgressChanged(total_usec, current_usec);
}

}       // namespace
