/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLOOPER_TRACK_WIDGET_HPP_
#define _RGLOOPER_TRACK_WIDGET_HPP_

#include <QWidget>
#include <QPushButton>
#include <QComboBox>

#include "../QLooper.h"
#include "TrackInputWidget.h"
#include "TrackStatusWidget.h"
#include "TrackOutputWidget.h"

namespace rg_looper {

class TrackWidget: public QWidget {
  Q_OBJECT

public:
  explicit TrackWidget(QLooper& q_looper, int track_id, QWidget* parent = 0);
  virtual ~TrackWidget() = default;

public slots:
  void TrackInputStateChanged(Track::InputState state);
  void TrackOutputStateChanged(Track::OutputState state);
  void TrackConnectedInputChanged(const std::list<std::string>& connected_inputs);
  void TrackConnectedOutputChanged(const std::list<std::string>& connected_outputs);
  void TrackProgressChanged(unsigned long total_usec, unsigned long current_usec);

private slots:
  void DeleteTrack();
  void AudioPortsScanned(const std::list<std::string>& input_ports, const std::list<std::string>& output_ports);

private:
  QLooper& _q_looper;
  int _track_id;
  TrackInputWidget* _inputs_widget;
  TrackStatusWidget* _status_widget;
  TrackOutputWidget* _output_widget;

  void FillCombobox(QComboBox& combo_box, const std::list<std::string>& ports);
};

}       // namespace
#endif  // _RGLOOPER_TRACK_WIDGET_HPP_
