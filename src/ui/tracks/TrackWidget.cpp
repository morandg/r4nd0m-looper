/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QGridLayout>
#include <QLabel>
#include <QGroupBox>

#include "../../logging/LogMacros.h"
#include "TrackWidget.h"

namespace rg_looper {

TrackWidget::TrackWidget(QLooper& q_looper, int track_id, QWidget* parent):
        QWidget(parent),
        _q_looper(q_looper),
        _track_id(track_id),
        _inputs_widget(new TrackInputWidget(q_looper, track_id, this)),
        _status_widget(new TrackStatusWidget(this)),
        _output_widget(new TrackOutputWidget(q_looper, track_id, this)) {
  QGroupBox* group_box = new QGroupBox(QString("Track ") + QString::number(track_id), this);
  QGridLayout* group_box_layout = new QGridLayout(group_box);
  QGridLayout* main_layout = new QGridLayout(this);

  QPushButton* remove_button = new QPushButton("Remove", this);
  group_box_layout->addWidget(remove_button, 0, 1);
  connect(remove_button, &QPushButton::clicked,
          this, &TrackWidget::DeleteTrack);

  group_box_layout->addWidget(_inputs_widget, 1, 0);
  _inputs_widget->setMinimumWidth(200);
  group_box_layout->addWidget(_status_widget, 1, 1);
  group_box_layout->addWidget(_output_widget, 1, 2);
  _output_widget->setMinimumWidth(200);

  group_box->setLayout(group_box_layout);

  main_layout->addWidget(group_box);
  setLayout(main_layout);

  connect(&_q_looper, &QLooper::AudioPortsScanned,
          this, &TrackWidget::AudioPortsScanned);
}

void TrackWidget::TrackInputStateChanged(Track::InputState state) {
  _inputs_widget->TrackInputStateChanged(state);
}

void TrackWidget::TrackOutputStateChanged(Track::OutputState state) {
  _output_widget->TrackOutputStateChanged(state);
}

void TrackWidget::TrackConnectedInputChanged(const std::list<std::string>& connected_inputs) {
  _inputs_widget->TrackConnectedInputChanged(connected_inputs);
}

void TrackWidget::TrackConnectedOutputChanged(const std::list<std::string>& connected_outputs) {
  _output_widget->TrackConnectedOutputChanged(connected_outputs);
}

void TrackWidget::TrackProgressChanged(unsigned long total_usec, unsigned long current_usec) {
  _status_widget->ProgressChanged(total_usec, current_usec);
}

void TrackWidget::DeleteTrack() {
  _q_looper.RemoveTrack(_track_id);
}

void TrackWidget::AudioPortsScanned(
        const std::list<std::string>& input_ports, const std::list<std::string>& output_ports) {
  _inputs_widget->AudioPortsScanned(input_ports);
  _output_widget->AudioPortsScanned(output_ports);
}

void TrackWidget::FillCombobox(QComboBox& combo_box, const std::list<std::string>& ports) {
  combo_box.clear();
  for(auto& input_port: ports) {
    combo_box.addItem(input_port.c_str());
  }
}

}       // namespace
