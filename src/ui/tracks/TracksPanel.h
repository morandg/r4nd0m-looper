/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLOOPER_TRACKS_PANEL_HPP_
#define _RGLOOPER_TRACKS_PANEL_HPP_

#include <QWidget>
#include <QVBoxLayout>
#include <QTimer>

#include "../QLooper.h"
#include "TrackWidget.h"

namespace rg_looper {

class TracksPanel: public QWidget {
  Q_OBJECT

public:
  explicit TracksPanel(QLooper& q_looper, QWidget* parent = 0);
  virtual ~TracksPanel() = default;

private slots:
  void TrackAdded(int track_id);
  void TrackRemoved(int track_id);
  void TrackInputStateChanged(int track_id, Track::InputState state);
  void TrackOutputStateChanged(int track_id, Track::OutputState state);
  void ConnectedInputchanged(int track_id, const std::list<std::string>& connected_input);
  void ConnectedOutputchanged(int track_id, const std::list<std::string>& connected_output);
  void ConnectedToJack();
  void JackConnectionClosed();
  void TrackProgressChanged(int track_id, unsigned long total_usec, unsigned long current_usec);

private:
  QLooper& _q_looper;
  QTimer* _refresh_tracks_timer;
  QVBoxLayout* _tracks_layout;
  QHash<int, TrackWidget*> _tracks;
};

}       // namespace
#endif  // _RGLOOPER_Q_JACK_CLIENT_HPP_
