set(APP_NAME ${PROJECT_NAME})

set(APP_SOURCES
    logging/ILogSink.cpp
    logging/LogStream.cpp
    logging/Logger.cpp
    logging/console/LoggerConsole.cpp
    logging/LoggerInstance.cpp
    engine/jack/JackPort.cpp
    engine/jack/JackClient.cpp
    engine/AudioBuffer.cpp
    engine/Track.cpp
    engine/TracksPool.cpp
    engine/Looper.cpp
    ui/events/LooperEventShutdown.cpp
    ui/events/LooperEventsQueue.cpp
    ui/events/LooperEventTrackRemoved.cpp
    ui/events/LooperEventTrackAdded.cpp
    ui/events/LooperEventTrackInputStateChanged.cpp
    ui/events/LooperEventTrackOutputStateChanged.cpp
    ui/events/LooperEventAudioPortsScanned.cpp
    ui/events/LooperEventConnectedInputChanged.cpp
    ui/events/LooperEventConnectedOutputChanged.cpp
    ui/events/LooperEventTrackProgressChanged.cpp
    ui/QLooper.cpp
    ui/tracks/TrackInputWidget.cpp
    ui/tracks/TrackWidget.cpp
    ui/tracks/TrackStatusWidget.cpp
    ui/tracks/TracksPanel.cpp
    ui/tracks/TrackOutputWidget.cpp
    ui/MainWindow.cpp
    R4nd0mLooper.cpp
    main.cpp)

find_package(PkgConfig REQUIRED)

pkg_search_module(JACKD REQUIRED jack)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

find_package(Qt5Widgets REQUIRED)
find_package(Qt5Gui REQUIRED)
find_package(Qt5Core REQUIRED)

include_directories(
    ${JACKD_INCLUDE_DIRS})
link_directories(
    ${JACKD_LIBRARY_DIRS})

add_executable(
    ${APP_NAME}
    ${APP_SOURCES})
target_link_libraries(
    ${APP_NAME}
    ${JACKD_LIBRARIES})
qt5_use_modules(${APP_NAME} Widgets)

install(TARGETS ${PROJECT_NAME} DESTINATION bin/)
