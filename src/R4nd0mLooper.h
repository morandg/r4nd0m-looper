/**
 * R4nd0m6uy looper
 *
 * Copyright (C) 2020 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLOOPER_R4ND0M_LOOPER_HPP_
#define _RGLOOPER_R4ND0M_LOOPER_HPP_

#include <QApplication>

#include "ui/MainWindow.h"

namespace rg_looper {

class R4nd0mLooper {
public:
   explicit R4nd0mLooper(int &argc, char **argv);
   virtual ~R4nd0mLooper() = default;

   int Run();

private:
    QApplication _qt_application;
    Looper _looper;
    MainWindow _main_window;
};

}       // namespace
#endif  // _RGLOOPER_R4ND0M_LOOPER_HPP_
